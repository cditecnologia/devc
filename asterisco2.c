#include <stdio.h>
#include <string.h>
#define ARG1 "-a"
#define ARG2 "-b"
#define ARG3 "-c"
#define ARG4 "-d"

void letraA();
void letraB();
void letraC();
void letraD();
void imprime();

void letraD(){
  char string[11] = {' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','\0'};
  for (int i = 9; i >=0; i--){
    string[i] = '*';
    imprime(string);
  }
}

void letraC(){
  char string[11] = {'*','*','*','*','*','*','*','*','*','*','\0'};
  for (int i = 0; i <=9; i++){
    imprime(string);
    string[i] = ' ';
  }
}

void letraB(){
  char string[11] = {'*','*','*','*','*','*','*','*','*','*','\0'};
  for (int i = 9; i >=0; i--){
    imprime(string);
    string[i] = ' ';
  }
}

void letraA(){
  char string[11] = {' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','\0'};
  for (int i = 0; i <=9; i++){
    string[i] = '*';
    imprime(string);
  }
}

void imprime(char string[]){
   printf("%s\n",string);
}

int main(int argc, char *argv[]){
   if (argc > 1){
      if ( strcmp(argv[1],ARG1) == 0 ){
         letraA();
         return 0;
      }

      if ( strcmp(argv[1],ARG2) == 0 ){
         letraB();
         return 0;
      }

      if ( strcmp(argv[1],ARG3) == 0 ){
         letraC();
         return 0;
      }

      if ( strcmp(argv[1],ARG4) == 0 ){
         letraD();
         return 0;
      }
   } else
      return 0;
}
