#ifndef MYLIB_H_INCLUDED
#define MYLIB_H_INCLUDED

#define STRING "*"
#define ESPACO " "
#define ARG1 "-a"
#define ARG2 "-b"
#define ARG3 "-c"
#define ARG4 "-d"

void testeA();
void testeB();
void testeC();
void testeD();

void testeD(){
   for (int x=1;x<=10;x++){
      for(int z=9;z>=x;z--){
         printf(ESPACO);
      }
      for( int y=1;y<=x;y++){
         printf(STRING);
      }
      puts("");
   }
}

void testeC(){
   for (int x=1;x<=10;x++){
      for(int z=2;z<=x;z++){
         printf(ESPACO);
      }
      for(int y=10;y>=x;y--){
         printf(STRING);
      }
      puts("");
   }
}

void testeB(){
   for (int x=10;x>0;x--){
      for(int y=1;y<=x;y++){
         printf(STRING);
      }
      puts("");
   }
}

void testeA(){
   for (int x=1;x<=10;x++){
      for(int y=1;y<=x;y++){
         printf(STRING);
      }
      puts("");
   }
}

#endif // MYLIB_H_INCLUDED