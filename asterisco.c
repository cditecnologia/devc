#include <stdio.h>
#include <string.h>
#include "mylib.h"


int main(int argc, char *argv[]){
   if (argc > 1){
      if ( strcmp(argv[1],ARG1) == 0 ){
         testeA();
         return 0;
      }

      if ( strcmp(argv[1],ARG2) == 0 ){
         testeB();
         return 0;
      }

      if ( strcmp(argv[1],ARG3) == 0 ){
         testeC();
         return 0;
      }

      if ( strcmp(argv[1],ARG4) == 0 ){
         testeD();
         return 0;
      }
   } else
      return 0;
}
